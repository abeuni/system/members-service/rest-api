import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, BaseEntity } from 'typeorm'
import EventCategory from './event_categories.model'
import MemberEvent from './members_events_join.model'

@Entity({ name: 'events' })
export default class Event extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => EventCategory, mgroup => mgroup.events)
  category: EventCategory;

  @Column()
  name: string;

  @Column({ nullable: true })
  code: string;

  @Column('date')
  startDate: Date;

  @Column('date', { nullable: true })
  endDate: Date;

  @Column('point', { nullable: true })
  location: number[]

  @Column('text', { nullable: true })
  description: string;

  @OneToMany(() => MemberEvent, mevent => mevent.event)
  members: MemberEvent;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}

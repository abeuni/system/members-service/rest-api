import { Entity, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, BaseEntity, PrimaryColumn } from 'typeorm'
import Member from './members.model'
import Event from './events.model'

@Entity({ name: 'members_events_history' })
export default class MemberEvent extends BaseEntity {
  @PrimaryColumn('uuid')
  @ManyToOne(() => Event, event => event.members)
  event: Event;

  @PrimaryColumn('uuid')
  @ManyToOne(() => Member, member => member.groups)
  member: Member;

  @Column('date')
  startDate: Date;

  @Column('date', { nullable: true })
  endDate: Date;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}

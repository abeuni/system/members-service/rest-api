import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToMany, BaseEntity } from 'typeorm'
import { IsEmail, IsPhoneNumber, Matches } from 'class-validator'

import MemberGroup from './members_groups_join.model'

@Entity({ name: 'members' })
export default class Member extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  fullname: string;

  @IsEmail()
  @Column({ unique: true })
  email: string;

  @Column({ unique: true })
  RG: string;

  @Matches(/^\d\d\d.\d\d\d.\d\d\d-\d\d$/)
  @Column({ unique: true })
  CPF: string;

  @Column('date')
  birthDate: Date;

  @IsPhoneNumber('BR')
  @Column()
  phone: string;

  @Column('json', { nullable: true })
  data: { [key: string]: string };

  @OneToMany(() => MemberGroup, mgroup => mgroup.member)
  groups: MemberGroup;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}

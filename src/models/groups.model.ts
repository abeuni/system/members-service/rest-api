import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, BaseEntity } from 'typeorm'
import MemberGroup from './members_groups_join.model'
import { Matches } from 'class-validator'
import { GroupCategory } from './enum.model.util'

@Entity({ name: 'groups' })
export default class Group extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('enum', { enum: GroupCategory })
  category: string;

  @Column({ unique: true })
  name: string;

  @Matches(/[A-Z]{3,8}/)
  @Column({ unique: true })
  code: string;

  @Column('text', { nullable: true })
  description: string;

  @OneToMany(() => MemberGroup, mgroup => mgroup.group)
  members: MemberGroup;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}

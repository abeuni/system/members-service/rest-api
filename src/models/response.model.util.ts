export const OK = <C> (data?: C): ResponseStatus<C> => {
  return {
    status: 'OK',
    data
  }
}
export const ERROR = <C> (error?: unknown): ResponseStatus<C> => {
  return {
    status: 'ERROR',
    error
  }
}

export type ResponseStatus<C> = SuccessStatus<C> | ErrorStatus<C>

interface SuccessStatus<C> {
  status: 'OK';
  data?: C;
}

interface ErrorStatus<C> {
  status: 'ERROR';
  error?: unknown;
}

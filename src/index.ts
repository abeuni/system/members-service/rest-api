import 'reflect-metadata'

import './orm'

import express from 'express'
import { Server } from 'typescript-rest'

import { logger } from './logging'
import { middlewares } from './schema'

// Configurations
const port = process.env.PORT || 3000

// Create App and Start listening
const app = express()

Server.buildServices(app)

middlewares.forEach((middleware) => {
  app.use(middleware)
})

app.listen(port, () => {
  logger.info(`Listening at ${port}`)
})

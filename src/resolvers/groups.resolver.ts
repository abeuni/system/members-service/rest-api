import { Path, GET, Param, Context, ServiceContext, Accept, PUT } from 'typescript-rest'
import Group from '@/models/groups.model'
import { ResponseStatus, OK, ERROR } from '@/models/response.model.util'
import { UpdateResult } from 'typeorm'

@Path('groups?')
@Accept('application/json')
export class GroupsResolver {
  @Context
  context: ServiceContext

  @GET
  async listGroups (
    @Param('skip') skipStart: string,
    @Param('limit') limitStart: string
  ): Promise<Group[]> {
    const skip = parseInt(skipStart || '0')
    const limit = parseInt(limitStart || '50')

    return Group.find({ skip, take: limit })
  }

  @PUT
  async updateGroup (
    data: {
      id: string,
      update: {
        name: string,
        code: string,
        description: string
      }
    }
  ): Promise<ResponseStatus<UpdateResult>> {
    try {
      const group = Group.update({ id: data.id }, {
        ...data.update
      })

      return OK(await group)
    } catch (e) {
      return ERROR(e)
    }
  }
}

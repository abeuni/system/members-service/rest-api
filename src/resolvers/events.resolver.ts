import { POST, Path, GET, Param, PUT } from 'typescript-rest'
import { validate } from 'class-validator'
import { ResponseStatus, OK, ERROR } from '@/models/response.model.util'
import Event from '@/models/events.model'
import { DateTime } from 'luxon'
import { UpdateResult } from 'typeorm'

@Path('events?')
export default class EventResolver {
  @GET
  async listEvents (
    @Param('skip') skipStart: string,
    @Param('limit') limitStart: string
  ): Promise<Event[]> {
    const skip = parseInt(skipStart || '0')
    const limit = parseInt(limitStart || '50')

    return Event.find({ skip, take: limit })
  }

  @POST
  async createEvent (
    data: {
      category: string,
      name: string,
      code?: string,
      startDate: string,
      endDate?: string,
      location?: {
        lat: number,
        lng: number
      },
      description?: string,
    }
  ): Promise<ResponseStatus<Event>> {
    const event = Event.create({
      name: data.name,
      code: data.code,
      startDate: DateTime.fromISO(data.startDate).toJSDate(),
      endDate: data.endDate && DateTime.fromISO(data.endDate).toJSDate(),
      location: data.location && [data.location.lat, data.location.lng],
      description: data.description
    })

    const errors = await validate(event)

    if (errors.length < 1) {
      return OK(await event.save())
    }

    return ERROR(errors)
  }

  @PUT
  async updateEvent (
    data: {
      id: string,
      update: {
        name?: string,
        code?: string,
        startDate?: string,
        endDate?: string,
        location?: {
          lat: number,
          lng: number
        },
        description?: string
      }
    }
  ): Promise<ResponseStatus<UpdateResult>> {
    const update = {
      name: data.update.name,
      code: data.update.code,
      startDate: data.update.startDate && DateTime.fromISO(data.update.startDate).toJSDate(),
      endDate: data.update.endDate && DateTime.fromISO(data.update.endDate).toJSDate(),
      location: data.update.location && [data.update.location.lat, data.update.location.lng],
      description: data.update.description
    }

    for (const key in update) {
      if (update[key] === undefined) delete update[key]
    }

    console.log(update)

    try {
      const event = Event.update({ id: data.id }, update)

      return OK(await event)
    } catch (e) {
      return ERROR(e)
    }
  }
}

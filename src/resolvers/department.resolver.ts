import { POST, Path } from 'typescript-rest'
import Group from '@/models/groups.model'
import { GroupCategory } from '@/models/enum.model.util'
import { validate } from 'class-validator'
import { ResponseStatus, OK, ERROR } from '@/models/response.model.util'

@Path('departments?')
export default class DepartmentResolver {
  @POST
  async createDepartment (
    data: {
      name: string,
      code: string,
      description?: string
    }
  ): Promise<ResponseStatus<Group>> {
    const { name, code, description } = data

    const group = Group.create({
      category: GroupCategory.DEPARTMENT,
      name,
      code,
      description
    })

    const errors = await validate(group)

    if (errors.length < 1) {
      return OK(await group.save())
    }

    return ERROR(errors)
  }
}

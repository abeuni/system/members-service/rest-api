import { Path, GET, Param, POST, Context, ServiceContext, Accept, PUT } from 'typescript-rest'
import Member from '@/models/members.model'
import { DateTime } from 'luxon'
import { ResponseStatus, OK, ERROR } from '@/models/response.model.util'
import { UpdateResult } from 'typeorm'

@Path('members?')
@Accept('application/json')
export class MembersResolver {
  @Context
  context: ServiceContext

  @GET
  async listMembers (
    @Param('skip') skipStart: string,
    @Param('limit') limitStart: string
  ): Promise<Member[]> {
    const skip = parseInt(skipStart || '0')
    const limit = parseInt(limitStart || '50')

    return Member.find({ skip, take: limit })
  }

  @POST
  async createMember (
    data: {
      fullname: string,
      email: string,
      RG: string,
      CPF: string,
      birthDate: string,
      phone: string,
      data: { [key: string]: string }
    }
  ): Promise<ResponseStatus<Member>> {
    try {
      const member = Member.create({
        fullname: data.fullname,
        email: data.email,
        RG: data.RG,
        CPF: data.CPF,
        birthDate: DateTime.fromISO(data.birthDate).toJSDate(),
        phone: data.phone,
        data: data.data
      })

      return OK(await member.save())
    } catch (e) {
      return ERROR(e)
    }
  }

  @PUT
  async updateMember (
    data: {
      id: string,
      update: {
        fullname: string,
        email: string,
        RG: string,
        CPF: string,
        birthDate: string,
        phone: string,
        data: { [key: string]: string }
      }
    }
  ): Promise<ResponseStatus<UpdateResult>> {
    try {
      const member = Member.update({ id: data.id }, {
        ...data.update
      })

      return OK(await member)
    } catch (e) {
      return ERROR(e)
    }
  }
}
